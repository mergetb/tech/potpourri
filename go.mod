module gitlab.com/mergetb/tech/potpourri

go 1.16

require (
	github.com/h2non/filetype v1.1.3
	github.com/klauspost/compress v1.15.4
	github.com/klauspost/pgzip v1.2.5
	github.com/sirupsen/logrus v1.8.1
	github.com/ulikunitz/xz v0.5.10
)
