# Potpourri

A collection of golang "primitives" used in MergeTB that:
- Multiple things need to use
- Are too general for inclusion into the API

This package should not contain any Golang packages with C wrappers, for compatability.
